import React, { PureComponent } from 'react';
import { Button, ButtonToolbar } from 'reactstrap';
import { fetchAsyncPublic, urlTalksRegister } from "../../../helpers/Globals/globals"
import LoadingComponent from "../../../helpers/LoadingComponent"
import PropTypes from 'prop-types';

class WizardFormTwo extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    previousPage: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.state = {
      showPassword: false,
      email: "",
      name: "",
      lastName: "",
      secondLastName: "",
      phoneNumber: "",
      secondPhoneNumber: "",
      isLoading: false
    };
  }


  validateData = () => {

    if (this.state.name == null || this.state.name == "") {
      alert("Atención! Debes ingresar un nombre.")

    } else if (this.state.lastName == null || this.state.lastName == "") {
      alert("Atención! Debes ingresar un apellido paterno.")
    }
    else if (this.state.secondLastName == null || this.state.secondLastName == "") {
      alert("Atención! Debes ingresar un apellido materno.")
    }
    else if ((this.state.email.includes("@") && this.state.email.includes(".")) != true) {
      alert("Atención! Debes ingresar un @  y un  punto (.), el correo : " + this.state.email + " esta incompleto")
    }
    else if (this.state.email == null || this.state.email == "") {
      alert("Atención! Debes ingresar un correo, ejemplo@mail.com")
    }
    else if (this.state.phoneNumber.length < 10) {
      alert("Atención! Debes ingresar un numero de teléfono minimo 10 digitos")
    }
    else if (this.state.secondPhoneNumber.length < 8) {
      alert("Atención! Debes ingresar un segundo numero de teléfono minimo 8 digitos")
    }
    else {
      this.setState({
        isLoading: true
      })
      this.registerTalk();
    }
  }


  registerTalk = () => {
    const { onSubmit, Schedule } = this.props;
    let body = {
      name: this.state.name,
      lastName: this.state.lastName,
      secondLastName: this.state.secondLastName,
      email: this.state.email,
      phoneNumber: this.state.phoneNumber,
      secondPhoneNumber: this.state.secondPhoneNumber,
    }
    fetchAsyncPublic(urlTalksRegister + Schedule._id, JSON.stringify(body), 'POST', '')
      .then(
        (result) => {
          if (result.success) {
            onSubmit(body)
          }else{
            this.setState({
              isLoading: false
            })
            alert("Ocurrio un error: Si ya te registraste con ese correo a nuestro sistema intenta con algún otro , recuerda que solo puedes hacer un registro por correo electrónico")
          }
        }
      )


  }



  showPassword = (e) => {
    e.preventDefault();
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    return (
      <div>
        {this.state.isLoading ? <LoadingComponent message="Registrando" /> :
          <form className="form form--horizontal wizard__form">
            <div className="form__form-group">
              <span className="form__form-group-label">Correo electrónico</span>
              <div className="form__form-group-field">
                <input
                  name="email"
                  className="form-control inputsRounded iconAlertext"
                  type="email"
                  onChange={(event) => {
                    this.setState({
                      email: event.target.value,
                    });
                  }}
                  value={this.state.email}
                  placeholder="ejemplo@mail.com"
                />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Nombre</span>
              <div className="form__form-group-field">
                <input
                  component="input"
                  className="form-control inputsRounded"
                  type="text"
                  onChange={(event) => {
                    this.setState({
                      name: event.target.value,
                    })
                  }}
                  defaultValue={this.state.name}
                />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Apellido paterno</span>
              <div className="form__form-group-field">
                <input
                  component="input"
                  className="form-control inputsRounded"
                  type="text"
                  onChange={(event) => {
                    this.setState({
                      lastName: event.target.value,
                    });
                  }}
                  value={this.state.lastName}
                />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Apellido materno</span>
              <div className="form__form-group-field">
                <input
                  component="input"
                  className="form-control inputsRounded"
                  type="text"
                  onChange={(event) => {
                    this.setState({
                      secondLastName: event.target.value,
                    });
                  }}
                  value={this.state.secondLastName}
                />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Teléfono (Celular 10 digitos)</span>
              <div className="form__form-group-field">
                <input
                  component="input"
                  className="form-control inputsRounded"
                  type="Number"
                  onChange={(event) => {
                    this.setState({
                      phoneNumber: event.target.value,
                    });
                  }}
                  value={this.state.phoneNumber}
                />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Otro teléfono de contacto</span>
              <div className="form__form-group-field">
                <input
                  component="input"
                  className="form-control inputsRounded"
                  type="Number"
                  onChange={(event) => {
                    this.setState({
                      secondPhoneNumber: event.target.value,
                    });
                  }}
                  value={this.state.secondPhoneNumber}
                />
              </div>
            </div>
            <ButtonToolbar className="form__button-toolbar wizard__toolbar">
              <Button color="primary" type="button" className="previous" onClick={this.props.previousPage}>Atras</Button>
              <Button color="primary" className="next" onClick={() => this.validateData()}>Registrar</Button>
            </ButtonToolbar>
          </form>
        }
      </div>
    );
  }
}


export default WizardFormTwo;
