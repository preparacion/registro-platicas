import React, { PureComponent } from 'react';
import { Button, ButtonToolbar, Card, CardBody, Col } from 'reactstrap';
import LoadingComponent from "../../../helpers/LoadingComponent"
import { fetchAsyncPublic, urlTalks } from "../../../helpers/Globals/globals"
import {to12,
  SetFechaTable,
  getDay} from "../../../helpers/Globals/functions"
import PropTypes from 'prop-types';

class WizardFormOne extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      groupTalks: [],
      loading: true,
      scheduleSelected: this.props.Schedule,
    }
  }

  componentDidMount() {
    this.cargarPlaticas();
  }

  cargarPlaticas = () => {
    fetchAsyncPublic(urlTalks, "", "GET", "")
      .then(
        (result) => {
          if (result.success) {
            this.setState({
              groupTalks: result.groups,
              loading: false
            })
          }
        }
      )

  }


  mostrarPlaticas = () => {
    return this.state.groupTalks.map((item) => {
      return (
        <Col md={6} xl={6} sm={12}>
          <Card className="pricing-card" onClick={() => this.setState({ scheduleSelected: item })}>
            <CardBody className={this.state.scheduleSelected?this.state.scheduleSelected._id == item._id ? "pricing-card__body pricing-card--info2" : "pricing-card__body":"pricing-card__body"}>
              <h3 className="pricing-card__plan">{getDay(item.schedules[0].day)}</h3>
              <h4 className="pricing-card__plan">{SetFechaTable(item.startDate)}</h4>
              <h4 className="pricing-card__plan">{to12(item.schedules[0].startHour)}</h4>
            </CardBody>
          </Card>
        </Col >
      )

    })
  }

  siguiente() {
    if (!this.state.scheduleSelected._id) {
      alert("selecciona un horario para continuar")
    } else {
      this.props.onSubmit(this.state.scheduleSelected)
    }
  }


  showPassword = (e) => {
    e.preventDefault();
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    const { onSubmit } = this.props;

    return (
      <form className="form form--horizontal wizard__form">
        <div className="form__form-group">
          <h3 className="wizard__title">Próxima platica informativa</h3>
        </div>
        <div className="form__form-group">
          <h5 className="wizard__title">Preparación IPN ofrecerá una plática informativa sin costo para todos los que harán el examen de admisión al Politécnico. Asiste y realiza un examen simulacro. Aprenderás técnicas para resolverlo más rápido. La plática durará 2 horas. </h5>
        </div>
        <div className="form__form-group">
          {this.state.loading ? <LoadingComponent message="Cargando platicas" /> :
            this.mostrarPlaticas()
          }
          {/* <Col md={6} xl={6} sm={12}>
            <Card className="pricing-card pricing-card--info">
              <CardBody className="pricing-card__body">
                <h3 className="pricing-card__plan">Sabado</h3>
                <h4 className="pricing-card__plan">17 de mayo</h4>
                <h4 className="pricing-card__plan">11:00 am</h4>
              </CardBody>
            </Card>
          </Col>

          <Col md={6} xl={6} sm={12} >
            <Card className="pricing-card pricing-card--info">
              <CardBody className="pricing-card__body">
                <h3 className="pricing-card__plan">Miercoles</h3>
                <h4 className="pricing-card__plan">21 de febrero</h4>
                <h4 className="pricing-card__plan">12:00 pm</h4>
              </CardBody>
            </Card>
          </Col> */}
        </div>

        {this.state.loading ? "" :
          <div className="form__form-group">
            <ButtonToolbar className="form__button-toolbar wizard__toolbar">
              <Button color="primary" className="next" onClick={() => this.siguiente()}>Siguiente</Button>
            </ButtonToolbar>
          </div>
        }
      </form >
    );
  }
}

export default WizardFormOne;
