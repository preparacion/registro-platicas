import React from 'react';
import { Button, ButtonToolbar } from 'reactstrap';
import { compose, withProps, withStateHandlers } from 'recompose';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps';
import { InfoBox } from 'react-google-maps/lib/components/addons/InfoBox';
import { generetePdf } from "./generatePdf"
import PropTypes from 'prop-types';


const MapWithAMarker = compose(
  withProps({
    // generate your API key
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7zTvQWuwyHZrjFE_L4rG6yfeV7OLsHG0' +
      '&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: '100%' }} />,
    containerElement: <div className="map" style={{ height: '360px', width: '100%' }} />,
    mapElement: <div style={{ height: '100%' }} />,
  }),
  withStateHandlers(() => ({
    isOpen: true,
  }), {
      onToggleOpen: ({ isOpen }) => () => ({
        isOpen: !isOpen,
      }),
    }),
  withScriptjs,
  withGoogleMap,
)(props => (
  <GoogleMap
    defaultZoom={16}
    defaultCenter={{ lat: 19.49677778, lng: -99.14250000 }}
  >
    {props.isMarkerShown &&
      <Marker position={{ lat: 19.49677778, lng: -99.14250000 }} onClick={props.onToggleOpen}>
        {props.isOpen &&
          <InfoBox options={{ closeBoxURL: '', enableEventPropagation: true }}>
            <div className="map__marker-label">
              <div className="map__marker-label-content">
                Preparación IPN<br />
                Av. Wilfrido Massieu 310
                Planetario Lindavista
                07739 Ciudad de México,
                México <br />
                Tel: (55) 8436 9486
              </div>
            </div>
          </InfoBox>}
      </Marker>}
  </GoogleMap>
));


const WizardFormOne = ({ handleSubmit, previousPage , bodyStudent, Schedule}) => (
  <form className="form form--horizontal wizard__form" onSubmit={handleSubmit}>
    <h3 className="wizard__title">¡Felicidades!</h3>
    <h5 className="wizard__title">Tus datos han sidos registrados correctamente en nuestro sistema y estás apunto de asistir a nuestra platica informativa.</h5>
    <h5 className="wizard__title">Generamos un comprobante de tu registro , puedes descargarla a continuación o más tarde en el correo electrónico que te hemos enviado.</h5>
    <div className="form__form-group">
      <Button color="primary" onClick={() => generetePdf(bodyStudent,Schedule)}>Descarga tu comprobante aquí</Button>
    </div>
    <span>Apunta nuestra dirección , aquí será la sede de tu platica informativa:</span>
    <div className="form__form-group">
      <MapWithAMarker isMarkerShown />
    </div>
  </form>
);

WizardFormOne.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  previousPage: PropTypes.func.isRequired,
};

export default WizardFormOne;
