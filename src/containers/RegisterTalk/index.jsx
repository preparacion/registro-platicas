import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import Wizard from './components/WizardForm';

const RegisterTalk = () => (

  <div className="account">
    <div className="account__wrapper">
      <div className="account__card__talk">
        <div className="account__head">
          <Container>
            <Row sm={12} className="center">
              <a className="account__img"/>
              <h3 className="account__title">PREPARACIÓN
            <span className="account__logo">IPN</span>
              </h3>
            </Row>
            <Row sm={12} className="center">
              <Col>
                <h3 className="page-title center">Registro de platica informativa</h3>
                <h3 className="page-subhead subhead center">¿Cómo ingresar al IPN?</h3>
              </Col>
            </Row>
          </Container>
        </div>
        <Container>
          <Wizard />
        </Container>
      </div>
      <div className="account__credits">
        <p>Copyrights © Preparación IPN</p>
      </div>
    </div>
  </div>
);

export default RegisterTalk;
