import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class MainWrapper extends PureComponent {
  static propTypes = {
    children: PropTypes.element.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      auth: false,
    };
  }
  componentDidMount() {
    if (this.state) { console.log(`estado de la variable es :${this.state.auth}`); }
    console.log("valor de el auth es:" + localStorage.getItem('auth') );
    if (localStorage.getItem('auth') === 'true') {
      let dataUser = JSON.parse(localStorage.getItem('dataLogin'));
      if (dataUser != null) {
        let auth = 'Bearer ' + dataUser.token
        console.log(auth)
      }
    }
  }
  render() {
    return (
      <div className="theme-light">
        <div className="wrapper">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default MainWrapper;
