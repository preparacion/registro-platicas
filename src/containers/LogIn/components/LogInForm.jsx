import React, { PureComponent } from 'react';
import EyeIcon from 'mdi-react/EyeIcon';
import { Link } from 'react-router-dom';
import CheckBox from '../../../shared/components/form/CheckBox';

class LogInForm extends PureComponent {
  constructor() {
    super();
    this.state = {
      showPassword: false,
    };
  }

  showPassword = (e) => {
    e.preventDefault();
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    return (
      <form className="form">
        <div className="form__form-group">
          <span className="form__form-group-label">Tú correo electrónico</span>
          <div className="form__form-group-field">

            <input
              name="email"
              type="email"
              placeholder="hola@preparacionipn.com"
            />
          </div>
        </div>
        <div className="form__form-group">
          <span className="form__form-group-label">Tú contraseña</span>
          <div className="form__form-group-field">
            <input
              name="password"
              type={this.state.showPassword ? 'text' : 'password'}
              placeholder="Contraseña"
            />
            <button
              className={`form__form-group-button${this.state.showPassword ? ' active' : ''}`}
              onClick={e => this.showPassword(e)}
            ><EyeIcon />
            </button>
          </div>
          <div className="account__forgot-password">
            <a href="/">¿Olvidaste tu contraseña?</a>
          </div>
        </div>
        <div className="form__form-group">
          <div className="form__form-group-field">
            <CheckBox name="remember_me" label="Recordarme" onChange={() => {}} />
          </div>
        </div>
        <div className="account__btns">
          <Link className="btn btn-primary account__btn" to="/dashboard_default">Entrar</Link>
        </div>
      </form>
    );
  }
}

export default LogInForm;
