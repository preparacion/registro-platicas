import React from 'react';
import { Col } from 'reactstrap';
import Loader from "react-loader-spinner";

const LoadingComponent = ({ message }) => (
    <Col className="text-center">
        <p>{message}</p>
        <Loader type="ThreeDots" color="#somecolor" height={80} width={80} />
    </Col>
)


export default LoadingComponent