// =================================================
// === dev
// =================================================
const urlBase = 'https://dev.api.preparacionipn.mx';

// =================================================
// === PROD
// =================================================
// const urlBase = 'https://api.preparacionipn.mx';


// =================================================
// === Endpoints
// =================================================
const urlPeople = urlBase + '';
const urlLogin = urlBase + '/api/login';
//const urlLoginStudent = urlBase+'/api/st/login';
const urlLoginStudent = urlBase + '/api/login';
const urlLoginStudentRecover = urlBase + '/api/st/pass/recover';
const urlRegister = urlBase + '/api/students/register';
const urlStudentRegister = urlBase + '/api/students/register';
const urlStudenDelete = urlBase + '/api/students/';
const urlPermissions = urlBase + '/api/permissions/';
const urlUsers = urlBase + '/api/users/';
const urlMe = urlBase + '/api/me';
const urlStudents = urlBase + '/api/students/'; // ?limit=200';//&skip=skipid
const urlSchedules = urlBase + '/api/schedules';
const urlRoles = urlBase + '/api/roles/';
const urlGroups = urlBase + '/api/groups/'; // idgroup ; obtener detalles del grupo
const urlCourses = urlBase + '/api/courses/';
const urlCourseLevels = urlBase + '/api/course/levels/';
const urlCourseTypes = urlBase + '/api/course/types/';
const urlPayments = urlBase + '/api/payments/student/'; // :studentid
const urlSearchBy = urlBase + '/api/students/search/term?q='; // :valor
const urlSearchElastic = urlBase + '/api/search?q='; // :valor
const urlClassRoom = urlBase + '/api/classrooms/';// :classroomid
const urlPayment = urlBase + '/api/payments';
const urlOcupabilidad = urlBase + '/api/available/location';
const urlavailableCourses = urlBase + '/api/available/courses/';
const urlAttendance = urlBase + '/api/attendance';
const urlAttendanceGroup = urlBase + '/api/attendance/group/';
const urlComments = urlBase + '/api/comments/';
const urlAttendanceSingle = urlBase + '/api/attendance/single/';
const urlAttendanceStudent = urlBase + '/api/attendance/student/';
// var urlUpadte

const urlSedes = urlBase + '/api/locations/';
const urlClassrooms = urlBase + '/api/classrooms/location/';
const urlEditarClassrooms = urlBase + '/api/classrooms/';
const urlTemplates = urlBase + '/api/templates/';
const urlNotificationStudent = urlBase + '/api/notifications/student/';
const urlNotificationGroups = urlBase + '/api/notifications/group/';
const urlTalks = urlBase + "/api/unpr/talks/groups";
const urlTalksRegister = urlBase + "/api/unpr/talks/register/";

// =================================================
// =================================================


async function fetchAsync(_url, _body, _method, _header, _token) {

  let auth = '';
  if (localStorage.getItem('authStudent') === 'true') {
    let dataUser = JSON.parse(localStorage.getItem('dataLoginStudent'));
    if (dataUser != null) {
      auth = 'Bearer ' + dataUser.token
    }
  }

  if (_token !== undefined) {
    auth = _token;
  }

  // console.log('===== token ====: ', auth);

  let headerC = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With',
    // "Content-Type": "Accept",
    'Authorization': auth,
  });
  var data;

  if (_body === '') {
    let options = {
      method: _method,
      headers: headerC
    }
    data = await fetch(_url, options)
      .then(response => response.json())
      .then(data => data)
  } else {
    let options = {
      method: _method,
      headers: headerC,
      body: _body,
    }
    data = await fetch(_url, options)
      .then(response => response.json())
      .then(data => data)
  }

  return data;
}

async function fetchAsyncPublic(_url, _body, _method, _header, _token) {

  let headerC = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With',
  });
  var data;
  if (_body === '') {
    data = await (await fetch(_url, {
      method: _method,
      headers: headerC
    })).json();
  } else {
    data = await (await fetch(_url, {
      method: _method,
      headers: headerC,
      body: _body,
    })).json();
  }

  return data;
}

export {
  urlComments,
  urlStudenDelete,
  fetchAsync,
  fetchAsyncPublic,
  urlCourses,
  urlPeople,
  urlLogin,
  urlLoginStudent,
  urlLoginStudentRecover,
  urlRegister,
  urlPermissions,
  urlUsers,
  urlMe,
  urlStudents,
  urlSchedules,
  urlRoles,
  urlStudentRegister,
  urlGroups,
  urlPayments,
  urlSearchBy,
  urlSearchElastic,
  urlClassRoom,
  urlBase,
  urlPayment,
  urlOcupabilidad,
  urlavailableCourses,
  urlAttendance,
  urlAttendanceGroup,
  urlEditarClassrooms,
  urlSedes,
  urlClassrooms,
  urlAttendanceSingle,
  urlAttendanceStudent,
  urlCourseLevels,
  urlCourseTypes,
  urlTemplates,
  urlNotificationGroups,
  urlNotificationStudent,
  urlTalks,
  urlTalksRegister,
};
