function getDay(id) {
    const days = ["Sabado", "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes"]
    return days[id];
}

function SetFechaTable(fecha) {

    if (fecha) {

        let fechaS = fecha.split('T')[0];
        let dia = fechaS.split('-')[2];
        let mes = fechaS.split('-')[1];
        let anio = fechaS.split('-')[0];

        return (`${dia}-${mes}-${anio}`)

    } else {
        return ''
    }
}

function to12(time24) {
    var ts = time24;
    var H = +ts.substr(0, 1);
    var h = (H % 12) || 12;
    h = (h < 10) ? ("0" + h) : h;  // leading 0 at the left for 1 digit hours
    var ampm = H < 12 ? " AM" : " PM";
    ts = h + ':' + ts.substr(2, 3) + ampm;
    return ts;
};
export {
    to12,
    SetFechaTable,
    getDay

}